﻿using System.Windows;

namespace GameOfLife_FA91_Posner_Steffien
{
    /// <summary>
    /// Interaktionslogik für RulesView.xaml
    /// </summary>
    public partial class RulesView : Window
    {
        public RulesView()
        {
            InitializeComponent();
        }

        private void OpenIcons8Btn_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://icons8.de/");
        }

    }
}
