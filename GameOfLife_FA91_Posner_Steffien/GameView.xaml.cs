﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GameOfLife_FA91_Posner_Steffien
{
    /// <summary>
    /// Interaktionslogik für GameView.xaml
    /// </summary>
    public partial class GameView : Window
    {
        private const int DEFAULT_TIME_INTERVAL_INDEX = 1;
        private int[] timeIntervals = new int[] { 75, 125, 250, 500, 750, 1000, 2000, 3000 }; 
        private CountDownTimer gameTimer = new CountDownTimer();

        private int cellCountWidth;
        private int cellCountHeight;
        private int livingCells = 0;
        private int stepCount = 0;
        private Rectangle[,] cells;
        private bool toroidalMode;
        private Brush livingCellBrush;
        private Brush deadCellBrush;

        private bool shouldExitProgram = true;

        public GameView(int cellCountWidth, int cellCountHeight, bool generateRandomStartGeneration, bool toroidalMode, Color livingCellsColor, Color deadCellsColor)
        {
            InitializeComponent();
            this.stopBtn.IsEnabled = false;
            this.Closed += Window_Closed;
            this.Title = "Game of Life (" + cellCountWidth + " x " + cellCountHeight + ")";
            this.cellCountWidth = cellCountWidth;
            this.cellCountHeight = cellCountHeight;
            this.toroidalMode = toroidalMode;
            this.livingCellBrush = new SolidColorBrush(livingCellsColor);
            this.deadCellBrush = new SolidColorBrush(deadCellsColor);

            timerSlider.Maximum = timeIntervals.Length - 1;
            timerSlider.Value = DEFAULT_TIME_INTERVAL_INDEX;
            gameTimer.TimeChanged += () => timerLabel.Content = gameTimer.TimeLeftStr;
            gameTimer.CountDownFinished += countDownFinished;
            gameTimer.StepMs = 77;

            cells = new Rectangle[cellCountHeight, cellCountWidth];
            createCells(generateRandomStartGeneration);
        }

        private void createCells(bool generateRandomStartGeneration)
        {
            livingCells = 0;
            var randomInt = new Random();
            for (int h = 0; h < cellCountHeight; h++)
            {
                for (int w = 0; w < cellCountWidth; w++)
                {
                    Rectangle rectangle = new Rectangle();
                    rectangle.Name = "Rectangle_" + h + "_" + w;
                    rectangle.Width = gameField.Width / cellCountWidth;
                    rectangle.Height = gameField.Height / cellCountHeight;
                    rectangle.Stroke = Brushes.White;
                    rectangle.StrokeThickness = 1;
                    rectangle.MouseDown += Rectangle_MouseDown;

                    bool cellIsAlive = false;
                    if (generateRandomStartGeneration)
                    {
                        cellIsAlive = Convert.ToBoolean(randomInt.Next(2));
                    }

                    if (cellIsAlive)
                    {
                        rectangle.Fill = livingCellBrush;
                        livingCells++;
                    }
                    else
                    {
                        rectangle.Fill = deadCellBrush;
                    }

                    gameField.Children.Add(rectangle);

                    Canvas.SetLeft(rectangle, w * rectangle.Width);
                    Canvas.SetTop(rectangle, h * rectangle.Height);

                    cells[h, w] = rectangle;
                }
            }

            updateCellsLabel();
        }

        private void updateCellsLabel()
        {
            cellsLabel.Content = livingCells + "/" + cellCountWidth * cellCountHeight;
        }

        private void updateStepCountLabel()
        {
            stepCountLabel.Content = stepCount;
        }

        private void updateTimerLabel(TimeSpan timeSpan)
        {
            timerLabel.Content = string.Format("{0:D2}.{1:D3}",
                                        timeSpan.Seconds,
                                        timeSpan.Milliseconds);
        }

        private void calculateAndUpdateNextGeneration()
        {
            int[,] livingNeighboursArray = countAllLivingNeighbours();

            livingCells = 0;
            for (int h = 0; h < cellCountHeight; h++)
            {
                for (int w = 0; w < cellCountWidth; w++)
                {
                    int livingNeighbours = livingNeighboursArray[h, w];
                    Rectangle rectangle = cells[h, w];

                    if (livingNeighbours < 2 || livingNeighbours > 3)
                    {
                        rectangle.Fill = deadCellBrush;
                    }
                    else if (rectangle.Fill == deadCellBrush && livingNeighbours == 3)
                    {
                        rectangle.Fill = livingCellBrush;
                    }

                    if (rectangle.Fill == livingCellBrush)
                    {
                        livingCells++;
                    }
                }
            }

            stepCount++;

            updateStepCountLabel();
            updateCellsLabel();
        }

        private int[,] countAllLivingNeighbours()
        {
            int[,] livingNeighboursArray = new int[cellCountHeight, cellCountWidth];
            for (int h = 0; h < cellCountHeight; h++)
            {
                for (int w = 0; w < cellCountWidth; w++)
                {
                    int livingNeighbours;
                    if (toroidalMode)
                    {
                        livingNeighbours = countLivingNeighboursForCellToroidal(h, w);
                    }
                    else
                    {
                        livingNeighbours = countLivingNeighboursForCellNonToroidal(h, w);
                    }
                    livingNeighboursArray[h, w] = livingNeighbours;
                }
            }

            return livingNeighboursArray;
        }

        private int countLivingNeighboursForCellToroidal(int h, int w)
        {
            int livingNeighbours = 0;

            int hTop = (h == 0 ? (cellCountHeight - 1) : h - 1);
            int hDown = (h == (cellCountHeight - 1) ? 0 : h + 1);
            int wLeft = (w == 0 ? (cellCountWidth - 1) : w - 1);
            int wRight = (w == (cellCountWidth - 1) ? 0 : w + 1);

            if (cells[hTop, wLeft].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }
            if (cells[hTop, w].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }
            if (cells[hTop, wRight].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }

            if (cells[h, wLeft].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }
            if (cells[h, wRight].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }

            if (cells[hDown, wLeft].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }
            if (cells[hDown, w].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }
            if (cells[hDown, wRight].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }

            return livingNeighbours;
        }

        private int countLivingNeighboursForCellNonToroidal(int h, int w)
        {
            int livingNeighbours = 0;

            int hTop = h - 1;
            int hDown = h + 1;
            int wLeft = w - 1;
            int wRight = w + 1;

            if (hTop >= 0 && hTop < cellCountHeight)
            {
                if (cells[hTop, w].Fill == livingCellBrush)
                {
                    livingNeighbours++;
                }
                if (wLeft >= 0 && wLeft < cellCountWidth && cells[hTop, wLeft].Fill == livingCellBrush)
                {
                    livingNeighbours++;
                }
                if (wRight >= 0 && wRight < cellCountWidth && cells[hTop, wRight].Fill == livingCellBrush)
                {
                    livingNeighbours++;
                }
            }
            if (hDown >= 0 && hDown < cellCountHeight)
            {
                if (cells[hDown, w].Fill == livingCellBrush)
                {
                    livingNeighbours++;
                }
                if (wLeft >= 0 && wLeft < cellCountWidth && cells[hDown, wLeft].Fill == livingCellBrush)
                {
                    livingNeighbours++;
                }
                if (wRight >= 0 && wRight < cellCountWidth && cells[hDown, wRight].Fill == livingCellBrush)
                {
                    livingNeighbours++;
                }
            }
            if (wLeft >= 0 && wLeft < cellCountWidth && cells[h, wLeft].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }
            if (wRight >= 0 && wRight < cellCountWidth && cells[h, wRight].Fill == livingCellBrush)
            {
                livingNeighbours++;
            }

            return livingNeighbours;
        }

        private void startGame()
        {
            generateNewGenerationBtn.IsEnabled = false;
            clearFieldBtn.IsEnabled = false;
            stepBtn.IsEnabled = false;
            startBtn.IsEnabled = false;
            stopBtn.IsEnabled = true;

            gameTimer.Start();
        }

        private void stopGame()
        {
            generateNewGenerationBtn.IsEnabled = true;
            clearFieldBtn.IsEnabled = true;
            stepBtn.IsEnabled = true;
            startBtn.IsEnabled = true;
            stopBtn.IsEnabled = false;

            gameTimer.Stop();
            updateTimerLabel(gameTimer.maxTimeSpan);
        }

        /* EVENTS */

        private void Window_Closed(object sender, EventArgs e)
        {
            gameTimer.Stop();
            gameTimer.Dispose();

            GameMenu.shouldExitProgram = shouldExitProgram;
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Rectangle rectangle = (Rectangle)sender;
            if (rectangle.Fill == livingCellBrush)
            {
                rectangle.Fill = deadCellBrush;
                livingCells--;
            }
            else
            {
                rectangle.Fill = livingCellBrush;
                livingCells++;
            }

            updateCellsLabel();
        }

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            shouldExitProgram = false;
            this.Close();

            Owner.ShowDialog();
        }

        private void ShowRulesBtn_Click(object sender, RoutedEventArgs e)
        {
            RulesView rulesView = new RulesView();
            rulesView.Owner = this;
            rulesView.ShowDialog();
        }

        private void OpenWikiBtn_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.conwaylife.com/wiki/Main_Page");
        }

        private void GenerateNewGenerationBtn_Click(object sender, RoutedEventArgs e) 
        {
            stepCount = 0;
            updateStepCountLabel();

            livingCells = 0;

            var rand = new Random();
            for (int h = 0; h < cellCountHeight; h++)
            {
                for (int w = 0; w < cellCountWidth; w++)
                {
                    if (rand.NextDouble() > 0.5)
                    {
                        cells[h, w].Fill = livingCellBrush;
                        livingCells++;
                    }
                    else
                    {
                        cells[h, w].Fill = deadCellBrush;
                    }
                }
            }

            updateCellsLabel();
        }

        private void ClearGameFieldBtn_Click(object sender, RoutedEventArgs e)
        {
            stepCount = 0;
            updateStepCountLabel();

            for (int h = 0; h < cellCountHeight; h++)
            {
                for (int w = 0; w < cellCountWidth; w++)
                {
                    cells[h, w].Fill = deadCellBrush;
                }
            }

            livingCells = 0;
            updateCellsLabel();
        }

        private void StepBtn_Click(object sender, RoutedEventArgs e)
        {
            if (livingCells > 0)
            {
                calculateAndUpdateNextGeneration();
            } else
            {
                MessageBox.Show("Ohne lebendige Zellen kann kein Schritt simuliert werden.", "Keine lebende Zellen");
            }
        }

        private void StartSimulationBtn_Click(object sender, RoutedEventArgs e)
        {
            if (livingCells > 0)
            {
                startGame();
            } else
            {
                MessageBox.Show("Ohne lebendige Zellen kann kein Spiel simuliert werden.", "Keine lebende Zellen");
            }
        }

        private void StopSimulationBtn_Click(object sender, RoutedEventArgs e)
        {
            stopGame();
        }

        private void TimerSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int ms = (int) timeIntervals.GetValue((int) timerSlider.Value);
            TimeSpan timeSpan = TimeSpan.FromMilliseconds(ms);
            updateTimerLabel(timeSpan);
            gameTimer.SetTime(timeSpan);
        }

        private void countDownFinished()
        {
            calculateAndUpdateNextGeneration();
            gameTimer.Restart();
        }
    }
}
