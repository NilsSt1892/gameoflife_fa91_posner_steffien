﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Windows.Media;

namespace GameOfLife_FA91_Posner_Steffien
{
    /// <summary>
    /// Interaktionslogik für GameMenu.xaml
    /// </summary>
    public partial class GameMenu : Window
    {
        private readonly Color defaultLivingCellColor = Color.FromArgb(255, (byte)238, (byte)238, (byte)0);
        private readonly Color defaultDeadCellColor = Color.FromArgb(255, (byte)128, (byte)128, (byte)128);

        private Color livingCellColor;
        private Color deadCellColor;

        public static bool shouldExitProgram
        { get; set; }

        private GameView gameView;
        
        public GameMenu()
        {
            InitializeComponent();
            livingCellColor = defaultLivingCellColor;
            deadCellColor = defaultDeadCellColor;
            colorPickerLivingCells.SelectedColor = livingCellColor;
            colorPickerDeadCells.SelectedColor = deadCellColor;
        }

        public void GameViewClosed(object sender, EventArgs e)
        {
            gameView = null;
            if (shouldExitProgram)
            {
                Application.Current.Shutdown();
            }
        }

        private void ShowRulesBtn_Click(object sender, RoutedEventArgs e)
        {
            RulesView rulesView = new RulesView();
            rulesView.Owner = this;
            rulesView.ShowDialog();
        }

        private void StartGameBtn_Click(object sender, RoutedEventArgs e)
        {
            if (randomGenerationCheckBox.IsChecked.HasValue && toroidModeCheckBox.IsChecked.HasValue &&
                Int32.TryParse(rowCountTxtBox.Text, out int rowCount) && Int32.TryParse(columnCountTxtBox.Text, out int columnCount) && 
                rowCount > 0 && columnCount > 0) {
                gameView = new GameView(rowCount, columnCount, randomGenerationCheckBox.IsChecked.Value, toroidModeCheckBox.IsChecked.Value, livingCellColor, deadCellColor);
                gameView.Owner = this;
                gameView.Closed += GameViewClosed;
                this.Hide();
                gameView.ShowDialog();
            } else {
                MessageBox.Show("Bitte die Eingaben überprüfen.", "Fehlerhafte Eingabe");
            }
        }

        private void ResetLivingColorBtn_Click(object sender, RoutedEventArgs e)
        {
            livingCellColor = defaultLivingCellColor;
            colorPickerLivingCells.SelectedColor = livingCellColor;
        }

        private void ResetDeadColorBtn_Click(object sender, RoutedEventArgs e)
        {
            deadCellColor = defaultDeadCellColor;
            colorPickerDeadCells.SelectedColor = deadCellColor;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ColorPickerLivingCells_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            livingCellColor = (Color)colorPickerLivingCells.SelectedColor;
        }

        private void ColorPickerDeadCells_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            deadCellColor = (Color)colorPickerDeadCells.SelectedColor;
        }
    }
}
