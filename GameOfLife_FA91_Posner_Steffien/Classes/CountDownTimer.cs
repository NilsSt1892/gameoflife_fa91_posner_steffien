﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace GameOfLife_FA91_Posner_Steffien
{
    public class CountDownTimer : IDisposable
    {
        public Stopwatch stopWatch = new Stopwatch();

        public Action TimeChanged;
        public Action CountDownFinished;

        public bool IsRunning => timer.Enabled;

        public int StepMs
        {
            get => timer.Interval;
            set => timer.Interval = value;
        }

        public TimeSpan maxTimeSpan = TimeSpan.FromMilliseconds(30000);

        public TimeSpan TimeLeft => (maxTimeSpan.TotalMilliseconds - stopWatch.ElapsedMilliseconds) > 0 ? TimeSpan.FromMilliseconds(maxTimeSpan.TotalMilliseconds - stopWatch.ElapsedMilliseconds) : TimeSpan.FromMilliseconds(0);

        public string TimeLeftStr => TimeLeft.ToString(@"ss\.fff");

        private Timer timer = new Timer();

        private bool _mustStop => (maxTimeSpan.TotalMilliseconds - stopWatch.ElapsedMilliseconds) < 0;

        public CountDownTimer()
        {
            Init();
        }

        public void SetTime(TimeSpan ts)
        {
            maxTimeSpan = ts;
            TimeChanged?.Invoke();
        }

        public void Start()
        {
            timer.Start();
            stopWatch.Start();
        }

        public void Pause()
        {
            timer.Stop();
            stopWatch.Stop();
        }

        public void Stop()
        {
            Reset();
            Pause();
        }

        public void Reset()
        {
            stopWatch.Reset();
        }

        public void Restart()
        {
            Reset();
            Start();
        }

        public void Dispose() => timer.Dispose();

        private void Init()
        {
            StepMs = 1000;
            timer.Tick += new EventHandler(TimerTick);
        }

        private void TimerTick(object sender, EventArgs e)
        {
            TimeChanged?.Invoke();

            if (_mustStop)
            {
                CountDownFinished?.Invoke();
            }
        }
    }
}
